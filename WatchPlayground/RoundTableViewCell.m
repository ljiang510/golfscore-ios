//
//  RoundTableViewCell.m
//  WatchPlayground
//
//  Created by Liangjun Jiang on 12/1/14.
//  Copyright (c) 2014 Liangjun Jiang. All rights reserved.
//

#import "RoundTableViewCell.h"
#import "Round.h"
#import "HoleScore.h"
@interface RoundTableViewCell()
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *courseLabel;
@property (nonatomic, weak) IBOutlet UILabel *scoreLabel;

@end

@implementation RoundTableViewCell

-(void)setCellItem:(Round *)round
{
    static NSDateFormatter *dateformatter = nil;
    if (dateformatter == NULL) {
        dateformatter = [[NSDateFormatter alloc] init];
        [dateformatter setDateStyle:NSDateFormatterMediumStyle];
    }
    self.dateLabel.text = [dateformatter stringFromDate:round.updatedAt];
    self.courseLabel.text = round.courseName?round.courseName:@"";
    int16_t totalScore = 0;
    for (HoleScore *holeScore in round.scores){
        totalScore +=holeScore.score;
    }
    self.scoreLabel.text = [NSString stringWithFormat:@"%d", totalScore];
    
}

@end
