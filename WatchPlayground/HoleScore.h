//
//  HoleScore.h
//  WatchPlayground
//
//  Created by Liangjun Jiang on 12/1/14.
//  Copyright (c) 2014 Liangjun Jiang. All rights reserved.
//

#import <Parse/Parse.h>

@interface HoleScore : PFObject<PFSubclassing>
@property (nonatomic, assign) int16_t score;
@property (nonatomic, assign) int16_t hole;

@end
