//
//  RoundTableViewCell.h
//  WatchPlayground
//
//  Created by Liangjun Jiang on 12/1/14.
//  Copyright (c) 2014 Liangjun Jiang. All rights reserved.
//

#import "PFTableViewCell.h"
@class Round;
@interface RoundTableViewCell : PFTableViewCell

-(void)setCellItem:(Round *)round;
@end
