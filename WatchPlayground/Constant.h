//
//  Constant.h
//  WatchPlayground
//
//  Created by Liangjun Jiang on 12/1/14.
//  Copyright (c) 2014 Liangjun Jiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
@interface Constant : NSObject

extern NSString *const kParseApplicationID;
extern NSString *const kParseClientKey;

@end
