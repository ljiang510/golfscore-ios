//
//  Round.h
//  WatchPlayground
//
//  Created by Liangjun Jiang on 12/1/14.
//  Copyright (c) 2014 Liangjun Jiang. All rights reserved.
//

#import "Constant.h"
@interface Round : PFObject<PFSubclassing>
@property (nonatomic, strong) PFUser *player;
@property (nonatomic, strong) NSArray *scores;
@property (nonatomic, copy) NSString *courseName;
+(PFQuery *)queryForUser:(PFUser *)user;
@end
