//
//  Round.m
//  WatchPlayground
//
//  Created by Liangjun Jiang on 12/1/14.
//  Copyright (c) 2014 Liangjun Jiang. All rights reserved.
//

#import "Round.h"
#import <Parse/PFObject+Subclass.h>
@implementation Round
@dynamic player, courseName, scores;
+(NSString *)parseClassName
{
    return @"Round";
}

+(PFQuery *)queryForUser:(PFUser *)user
{
    PFQuery *query = [self query];
    [query whereKey:@"player" equalTo:user];
    [query orderByDescending:@"updatedAt"];
    
    return query;
    
}
@end
