//
//  HoleScore.m
//  WatchPlayground
//
//  Created by Liangjun Jiang on 12/1/14.
//  Copyright (c) 2014 Liangjun Jiang. All rights reserved.
//

#import "HoleScore.h"
#import  <Parse/PFObject+Subclass.h>
@implementation HoleScore
@dynamic score, hole;

+(NSString *)parseClassName
{
    return @"HoleScore";
}
@end
