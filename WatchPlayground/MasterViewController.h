//
//  MasterViewController.h
//  WatchPlayground
//
//  Created by Liangjun Jiang on 11/19/14.
//  Copyright (c) 2014 Liangjun Jiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
@class DetailViewController;

@interface MasterViewController :PFQueryTableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

